package main

import (
	"fmt"

	"gitlab.com/smileycake/books/pkg/api"
	"gitlab.com/smileycake/books/pkg/domain"

	"github.com/eawsy/aws-lambda-go-core/service/lambda/runtime"
	"github.com/eawsy/aws-lambda-go-event/service/lambda/runtime/event/apigatewayproxyevt"
)

var service *domain.BookService

func init() {
	service = domain.NewBookService()
}

func main() {
	r, _ := service.FindBooks(api.HTTPRequestParams{})
	fmt.Println(r)
}

// Get is a singular get method
func Get(evt *apigatewayproxyevt.Event, _ *runtime.Context) (interface{}, error) {
	return service.GetBook(api.GetParams(evt))
}

// List is a get all method
func List(evt *apigatewayproxyevt.Event, _ *runtime.Context) (interface{}, error) {
	return service.FindBooks(api.GetParams(evt))
}

// Create adds a new item
func Create(evt *apigatewayproxyevt.Event, _ *runtime.Context) (interface{}, error) {
	return service.AddBook(api.GetParams(evt))
}

// func Get(evt *apigatewayproxyevt.Event, _ *runtime.Context) (interface{}, error) {
// 	return apigateway.HandleGet(evt, mockTodoService.Get)
// }

// func List(evt *apigatewayproxyevt.Event, _ *runtime.Context) (interface{}, error) {
// 	return apigateway.HandleList(evt, mockTodoService.List)
// }

// func Update(evt *apigatewayproxyevt.Event, _ *runtime.Context) (interface{}, error) {
// 	return apigateway.HandleUpdate(evt, todo.TodoAPIGAtewayEventUnmarshaler, mockTodoService.Update)
// }

// func Delete(evt *apigatewayproxyevt.Event, _ *runtime.Context) (interface{}, error) {
// 	return apigateway.HandleDelete(evt, mockTodoService.Delete)
// }
