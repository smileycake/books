package api

import "encoding/json"
import "net/http"

// HTTPResponse is a general api response
type HTTPResponse struct {
	StatusCode    int               `json:"statusCode"`
	Headers       map[string]string `json:"headers"`
	Body          interface{}       `json:"body,omitempty"`
	Base64Encoded bool              `json:"isBase64Encoded,omitempty"`
	ErrorType     string            `json:"errorType,omitempty"`
	ErrorMessage  string            `json:"errorMessage,omitempty"`
}

// NewHTTPResponse creates new response
func NewHTTPResponse(status int) *HTTPResponse {
	return &HTTPResponse{
		StatusCode:    status,
		Base64Encoded: false,
		Headers:       make(map[string]string),
	}
}

// SetBody checks to see if body implements json.Marshaler first
// If it does, it will use MarshalJSON() function.
// If not, defer back to json.MarshalIndent()
func (r *HTTPResponse) SetBody(b interface{}) *HTTPResponse {
	if b == nil {
		return r
	}
	if m, ok := b.(json.Marshaler); ok {
		bytes, _ := m.MarshalJSON()
		r.Body = string(bytes)
	} else {
		bytes, _ := json.MarshalIndent(b, "", "")
		r.Body = string(bytes)
	}
	return r
}

// SetError attaches the error provided
func (r *HTTPResponse) SetError(err error) *HTTPResponse {
	r.ErrorType = http.StatusText(r.StatusCode)
	if err != nil {
		r.ErrorMessage = err.Error()
	} else {
		r.ErrorMessage = r.ErrorType
	}
	return r
}

// SuccessResponse creates a new success response
func SuccessResponse(status int, data interface{}) (*HTTPResponse, error) {
	return NewHTTPResponse(status).SetBody(data), nil
}

// ErrorResponse creates a new error response
func ErrorResponse(status int, err error) (*HTTPResponse, error) {
	return NewHTTPResponse(status).SetError(err), nil
}
