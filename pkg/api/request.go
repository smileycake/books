package api

import (
	"github.com/eawsy/aws-lambda-go-event/service/lambda/runtime/event/apigatewayproxyevt"
)

// HTTPRequestParams represent http request information
type HTTPRequestParams struct {
	// The incoming request path parameters corresponding to the resource
	// path placeholders values as defined in Resource.
	PathParameters map[string]string

	// The incoming request query string parameters.
	// Duplicate entries are not supported.
	QueryStringParameters map[string]string

	// If used with Amazon API Gateway binary support, it represents the
	// Base64 encoded binary data from the client.
	// Otherwise it represents the raw data from the client.
	Body string

	// A flag to indicate if the applicable request payload is Base64
	// encoded.
	IsBase64Encoded bool
}

// GetParams returns params object
func GetParams(evt *apigatewayproxyevt.Event) HTTPRequestParams {
	return HTTPRequestParams{
		PathParameters:        evt.PathParameters,
		QueryStringParameters: evt.QueryStringParameters,
		Body:            evt.Body,
		IsBase64Encoded: evt.IsBase64Encoded,
	}
}
