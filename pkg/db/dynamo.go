package db

import (
	"log"

	"gitlab.com/smileycake/books/pkg/config"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
	uuid "github.com/satori/go.uuid"
)

const (
	partitionKey = "ID"
)

type DynamoDBService struct {
	db    *dynamo.DB
	table *dynamo.Table
}

type QueryService interface {
	Get(id uuid.UUID) (interface{}, error)
	List(filter map[string]string) ([]interface{}, error)
}

type CommandService interface {
	Create(m interface{}) error
	Update(m interface{}) error
	Delete(id uuid.UUID) error
}

// NewDynamoDB creates new dynamo db connection
func NewDynamoDB(dbConf *config.DynamoDBConfig) *dynamo.DB {
	s, err := session.NewSession()
	if err != nil {
		log.Fatalf("failed to connect to database: %s", err)
	}

	awsConfig := aws.NewConfig()
	awsConfig.Region = aws.String(dbConf.AWSRegion)
	if dbConf.EndPoint != "" {
		awsConfig.Endpoint = aws.String(dbConf.EndPoint)
	}
	return dynamo.New(s, awsConfig)
}

func NewTodoDynamoDBService(dbConf *config.DynamoDBConfig) *DynamoDBService {
	db := NewDynamoDB(dbConf)
	table := db.Table(dbConf.Table)
	return &DynamoDBService{db: db, table: &table}
}

// func (s *DynamoDBService) Create(v interface{}) error {
// 	t := v.(*todo.Todo)
// 	return s.table.Put(t).Run()
// }

// func (s *DynamoDBService) Get(id uuid.UUID) (interface{}, error) {
// 	todo := &todo.Todo{}
// 	if err := s.table.Get(partitionKey, id).One(todo); err != nil {
// 		if err == dynamo.ErrNotFound {
// 			return nil, nil
// 		}
// 		return nil, err
// 	}
// 	return todo, nil
// }

// func (s *TodoDynamoDBService) List(_ map[string]string) ([]api.Entity, error) {
// 	return nil, nil
// }

// func (s *TodoDynamoDBService) Update(t api.Entity) error {

// 	return nil
// }

// func (s *TodoDynamoDBService) Delete(id [16]byte) error {
// 	return nil
// }
