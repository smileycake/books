package domain

import (
	uuid "github.com/satori/go.uuid"
)

// Book is a model
type Book struct {
	ID          uuid.UUID `json:"id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Categories  []string  `json:"categories"`
	AuthorID    uuid.UUID `json:"authorId"`
}

// NewBook creates a new book model
func NewBook(authorID [16]byte, title, description string, categories []string) *Book {
	return &Book{
		Title:       title,
		Description: description,
		Categories:  categories,
		AuthorID:    authorID,
	}
}
