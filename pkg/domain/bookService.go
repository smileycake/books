package domain

import (
	"gitlab.com/smileycake/books/pkg/api"

	"encoding/json"
	"net/http"
	"strings"

	uuid "github.com/satori/go.uuid"
)

// BookService is a crud service
type BookService struct {
	Books []Book
	//db    db.DataService
}

// NewBookService creates a new book crud service
func NewBookService() *BookService {
	return &BookService{
		Books: []Book{
			Book{
				ID:          uuid.NewV4(),
				Title:       "book 1",
				Description: "my book 1",
				Categories:  []string{"action"},
				AuthorID:    uuid.NewV4(),
			},
			Book{
				ID:          uuid.NewV4(),
				Title:       "book 2",
				Description: "my book 2",
				Categories:  []string{"thriller"},
				AuthorID:    uuid.NewV4(),
			},
		},
	}
}

// GetBook returns single book
func (s *BookService) GetBook(req api.HTTPRequestParams) (*api.HTTPResponse, error) {
	id, err := uuid.FromString(req.PathParameters["id"])
	if err != nil {
		return api.ErrorResponse(http.StatusBadRequest, nil)
	}

	for i := range s.Books {
		if s.Books[i].ID == id {
			return api.SuccessResponse(http.StatusOK, &s.Books[i])
		}
	}

	return api.ErrorResponse(http.StatusNotFound, nil)
}

// FindBooks returns list based on search
func (s *BookService) FindBooks(req api.HTTPRequestParams) (*api.HTTPResponse, error) {
	var books []Book
	if title, ok := req.QueryStringParameters["title"]; ok {
		for i := range s.Books {
			if strings.Contains(s.Books[i].Title, title) {
				books = append(books, s.Books[i])
			}
		}
	} else {
		books = make([]Book, len(s.Books))
		copy(books, s.Books)
	}
	return api.SuccessResponse(http.StatusOK, books)
}

// AddBook adds a book
func (s *BookService) AddBook(req api.HTTPRequestParams) (*api.HTTPResponse, error) {
	var b Book
	if err := json.Unmarshal([]byte(req.Body), &b); err != nil {
		return api.ErrorResponse(http.StatusUnprocessableEntity, err)
	}
	//db.
	return api.SuccessResponse(http.StatusOK, nil)
}
